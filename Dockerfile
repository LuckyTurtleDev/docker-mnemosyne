FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
  mnemosyne \
  sqlite3 \
  && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /usr/bin/entrypoint.sh

RUN useradd -m -u 1000 docker-user
USER docker-user

WORKDIR /data

CMD ["entrypoint.sh"]
